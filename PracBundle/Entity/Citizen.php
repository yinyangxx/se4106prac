<?php

namespace CpuSeDept\PracBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Citizen
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="CpuSeDept\PracBundle\Entity\CitizenRepository")
 */
class Citizen
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=255)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="string", length=255)
     */
    private $lastName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthdate", type="date")
     */
    private $birthdate;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=6)
     */
    private $gender;

    /**
     * @var string
     *
     * @ORM\Column(name="disability", type="string", length=255)
     */
    private $disability;

    /**
     * @var Town
     * @ORM\ManyToOne(targetEntity="Town", inversedBy="citizens")
     */
    private $hometown;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Citizen
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Citizen
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set birthdate
     *
     * @param \DateTime $birthdate
     * @return Citizen
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    /**
     * Get birthdate
     *
     * @return \DateTime 
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * Set gender
     *
     * @param string $gender
     * @return Citizen
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string 
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set disability
     *
     * @param string $disability
     * @return Citizen
     */
    public function setDisability($disability)
    {
        $this->disability = $disability;

        return $this;
    }

    /**
     * Get disability
     *
     * @return string 
     */
    public function getDisability()
    {
        return $this->disability;
    }

    /**
     * Set hometown
     *
     * @param \CpuSeDept\PracBundle\Entity\Town $hometown
     * @return Citizen
     */
    public function setHometown(\CpuSeDept\PracBundle\Entity\Town $hometown = null)
    {
        $this->hometown = $hometown;

        return $this;
    }

    /**
     * Get hometown
     *
     * @return \CpuSeDept\PracBundle\Entity\Town 
     */
    public function getHometown()
    {
        return $this->hometown;
    }
    
    public function isDisabled()
    {
        return $this->disability != "";
    }
    
    public function getAge()
    {
        $now = new \DateTime();
        return $now->diff($this->birthdate)->y;
    }
    
    public function isSenior() {
        return $this->getAge() >= 65;
    }
    
    public function isChild() {
        return $this->getAge() <= 12;
    }
}
